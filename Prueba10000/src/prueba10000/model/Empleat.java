/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Daniel
 */
public class Empleat {
    
    private String nom;
    private String nif;

    public Empleat(String nom, String nif) {
        this.nom = nom;
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public String getNif() {
        return nif;
    }

    @Override
    public String toString() {
        return "Empleat: " + nom;
    }
    
    public String AllToString(){
        return toString() + " Nif: " + nif;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
