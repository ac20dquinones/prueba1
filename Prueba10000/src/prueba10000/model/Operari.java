/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Daniel
 */
public class Operari extends Empleat{
    protected Torn torn;

    public Operari(Torn torn, String nom, String nif) {
        super(nom, nif);
        this.torn = torn;
    }

    public Torn getTorn() {
        return torn;
    }
    
    

    @Override
    public String toString() {
        return super.toString() + getClass();
    }
    
    
    @Override
    public String AllToString(){
        return super.AllToString() + " Torn: " + getTorn();
    }
    
}
