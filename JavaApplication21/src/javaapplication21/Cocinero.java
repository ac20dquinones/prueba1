package javaapplication21;


import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class Cocinero extends Thread {
    private Restaurante restaurante;

    public Cocinero(Restaurante restaurante) {
        this.restaurante = restaurante;
        this.start();
    }
    
    @Override
    public void run(){
        int i = 0;
        while (i<=10){
            try {
                restaurante.cocinar();
                Thread.sleep(1000);
                i++;
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    
}
