package javaapplication21;


import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class Restaurante {

    final private String CortandoIngredientes = "Cortando ingredientes";
    final private String Cocinando = "Cocinando";
    final private String ComidaLista = "** COMIDA LISTA **";

    final private String TomandoNota = "Tomando Nota";
    final private String SirviendoBebidas = "Sirviendo Bebidas";
    final private String SirviendoComida = "** SIRVIENDO COMIDA **";
    final private String EsperandoPlatos = "Esperando Platos";

    private boolean valorEscrito = false;

    public synchronized void cocinar() throws InterruptedException {
        if (!this.valorEscrito) {
            try {
                this.wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
        System.out.println("[Cocinero] " + CortandoIngredientes);
        Thread.sleep(1000);
        System.out.println("[Cocinero] " + Cocinando);
        Thread.sleep(1000);
        System.out.println("[Cocinero] " + ComidaLista);
        
        this.valorEscrito = false;
        this.notify();
        
    }

    public synchronized void servir() throws InterruptedException {
        if (this.valorEscrito) {

            try {
                this.wait();
            } catch (InterruptedException e) {
            }
        }
        System.out.println("[Camarero] " + TomandoNota);
        Thread.sleep(1000);
        System.out.println("[Camarero] " + SirviendoBebidas);
        Thread.sleep(1000);
        System.out.println("[Camarero] " + EsperandoPlatos);       
        Thread.sleep(1000);
        System.out.println("[Camarero] " + SirviendoComida);
        Thread.sleep(1000);
        this.valorEscrito = true;
        this.notify();
        
    }

}
